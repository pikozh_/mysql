package repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SQLRepository {

    private static final String URL = "jdbc:mysql://localhost:3306/people?useUnicode=true&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASSWORD = "Lokolol12345";
    private Statement statement;
    private ResultSet resultSet;
    private Connection connection;


    public SQLRepository() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            if (connection != null) {
                System.out.println("Connected to the database test1");
            }
        } catch (ClassNotFoundException e) {
            System.err.format("Driver error");
            e.printStackTrace();
        } catch (SQLException e) {
            System.err.format("Connection error");
        }

        createTables();
    }

    public void insertDefault3Persons() throws SQLException {
        String queryForPeople = "INSERT INTO people.persons (firstName, lastName , age, idStreet) \n" +
                " VALUES ('Kathy', 'Sieara', 16, NULL),('Leo', 'Torn', 12, 2), ('Max', 'Ukupnik', 17, 2);";

        String queryForStreet = "INSERT INTO people.street (name) VALUES ('Lobanovskogo'), ('Pravdi Avenue'), ('Bazhan');";

        statement.executeUpdate(queryForStreet);
        statement.executeUpdate(queryForPeople);
    }

    public int getCountOfAllPeople() throws SQLException {
        int count = 0;
        String querry = "SELECT COUNT(*) FROM persons";

        statement = connection.createStatement();
        resultSet = statement.executeQuery(querry);
        while (resultSet.next()) {
            count = resultSet.getInt(1);
        }
        statement.close();
        return count;
    }

    public int averageAgeOfPeople() throws SQLException {
        String query = "SELECT avg(age) FROM persons";
        int count = 0;

        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            count = resultSet.getInt(1);
        }
        statement.close();
        return count;
    }

    public List<String> peopleSortedByLastName() throws SQLException {
        String query = "SELECT DISTINCT lastName FROM persons ORDER BY lastName ASC";
        List<String> listByLastName = new ArrayList<>();

        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            listByLastName.add(resultSet.getString(1));
        }
        statement.close();
        return listByLastName;
    }

    public Map<String, String> peopleCountedByLastName() throws SQLException {
        String query = "SELECT lastName, COUNT(*) FROM persons GROUP BY lastName";
        Map<String, String> map = new HashMap<>();

        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            map.put(resultSet.getString(1), resultSet.getString(2));
        }
        statement.close();
        return map;
    }

    public List<String> peopleWhoHasTheLetterBInTheMiddle() throws SQLException {
        String query = "SELECT lastName FROM persons WHERE lastName LIKE '_%б%_'";
        List<String> arrayList = new ArrayList<>();

        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            arrayList.add(resultSet.getString(1));

        }
        return arrayList;
    }

    public ArrayList<String> homelessPeople() throws SQLException {
        String query = "SELECT * FROM persons WHERE idStreet IS NULL";
        ArrayList<String> arrayList = new ArrayList<>();

        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            String resident = new StringBuilder()
                    .append("(id:")
                    .append(resultSet.getString(1))
                    .append(", ")
                    .append(resultSet.getString(2))
                    .append(" ")
                    .append(resultSet.getString(3))
                    .append(", age:")
                    .append(resultSet.getString(4))
                    .append(")")
                    .toString();
            arrayList.add(resident);
        }
        statement.close();
        return arrayList;
    }

    public List<String> listOfCitizensOnPravdiAvenue() throws SQLException {
        String query = "SELECT * FROM persons JOIN street ON persons.idStreet = street.id WHERE UPPER(name) =  'Pravdi avenue' AND persons.age < 18";
        List<String> arrayList = new ArrayList<>();

        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            String persons = new StringBuilder()
                    .append("(id:")
                    .append(resultSet.getString(1))
                    .append(", firstName: ")
                    .append(resultSet.getString(2))
                    .append(", lastName: ")
                    .append(resultSet.getString(3))
                    .append(")")
                    .toString();
            arrayList.add(persons);
        }

        return arrayList;
    }

    public Map<String, String> listOfAllStreetsAndCountPersonsOnStreet() throws SQLException {
        String query = "SELECT name, COUNT(persons.id) FROM street JOIN persons ON street.id = persons.idStreet GROUP BY name";
        Map<String, String> map = new HashMap<>();

        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            map.put(resultSet.getString(1), resultSet.getString(2));
        }
        return map;
    }

    public List listOfStreetsWithSixLetters() throws SQLException {
        String query = "SELECT * FROM street WHERE LENGTH(name) = 6";
        List<String> arrayList = new ArrayList<>();

        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            arrayList.add(resultSet.getString(2));
        }
        return arrayList;
    }

    public List<String> listOfStreetsWhereLessThanThreePersons() throws SQLException {
        String query = "SELECT name FROM street JOIN persons ON street.id = persons.idStreet GROUP BY name HAVING COUNT(persons.id) < 3";
        List<String> arrayList = new ArrayList<>();

        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            arrayList.add(resultSet.getString(1));
        }
        statement.close();
        return arrayList;
    }

    private void createTables() {
        try {
            statement = connection.createStatement();
            String createPersonsTable = "CREATE TABLE IF NOT EXISTS persons("
                    + "id   INT  AUTO_INCREMENT PRIMARY KEY,"
                    + "firstName VARCHAR (100)     NOT NULL,"
                    + "lastName VARCHAR(100) NOT NULL,"
                    + "age  INT        NOT NULL,"
                    + "idStreet INT," +
                    "FOREIGN KEY (idStreet) REFERENCES street (id));";

            String createStreetTable = "CREATE TABLE IF NOT EXISTS street(id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, name VARCHAR (100) NOT NULL);";

            statement.executeUpdate(createStreetTable);
            statement.executeUpdate(createPersonsTable);

            System.out.println("Tables successfully created");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
