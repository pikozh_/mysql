package repository;

import java.sql.SQLException;

public class Main {

    public static void main(String[] args) {
        SQLRepository sqlRepository = new SQLRepository();

        try {
            //sqlRepository.insertDefault3Persons();
            System.out.println("Сейчас будет выведено общее число жителей: " + sqlRepository.getCountOfAllPeople() + "\n");
            System.out.println("Сейчас будет выведен средний возраст жителей: " + sqlRepository.averageAgeOfPeople() + "\n");
            System.out.println("Сейчас будет выведен отсортированный по алфавиту список фамилий без повторений: " + sqlRepository.peopleSortedByLastName() + "\n");
            System.out.println("Сейчас будет выведен список фамилий, с указанием количества повторений этих фамилий в общем списке: " + sqlRepository.peopleCountedByLastName() + "\n");
            System.out.println("Сейчас будут выведены фамилии, которые содержат в середине букву «б»: " + sqlRepository.peopleWhoHasTheLetterBInTheMiddle() + "\n");
            System.out.println("Сейчас будет выведен  список «бомжей»: " + sqlRepository.homelessPeople() + "\n");
            System.out.println("Сейчас будет выведен список несовершеннолетних, проживающих на проспекте Правды: " + sqlRepository.listOfCitizensOnPravdiAvenue() + "\n");
            System.out.println("Сейчас будет выведен упорядоченный по алфавиту список всех улиц с указанием, сколько жильцов живёт на улице: " + sqlRepository.listOfAllStreetsAndCountPersonsOnStreet() + "\n");
            System.out.println("Сейчас будет выведен список улиц, название которых состоит из 6-ти букв: " + sqlRepository.listOfStreetsWithSixLetters() + "\n");
            System.out.println("Сейчас будет выведен список улиц с количеством жильцов на них меньше 3: " + sqlRepository.listOfStreetsWhereLessThanThreePersons() + "\n");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
