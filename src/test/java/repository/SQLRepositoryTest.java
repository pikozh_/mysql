package repository;

import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;

public class SQLRepositoryTest {

    SQLRepository sqlRepository = new SQLRepository();

    //    prepareForTesting();
    @Test
    public void getCountOfAllPeople_ShouldPass() throws SQLException {
        //when
        int expected = 3;
        int actual = sqlRepository.getCountOfAllPeople();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void averageAgeOfPeople_ShouldPass() throws SQLException {
        //when
        int expected = 15;
        int actual = sqlRepository.averageAgeOfPeople();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void peopleSortedByLastName_ShouldPass() throws SQLException {
        //when
        String expected = "[Sieara, Torn, Ukupnik]";
        String actual = sqlRepository.peopleSortedByLastName().toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void peopleCountedByLastName_ShouldPass() throws SQLException {
        //when
        String expected = "{Torn=1, Ukupnik=1, Sieara=1}";
        String actual = sqlRepository.peopleCountedByLastName().toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void peopleWhoHasTheLetterBInTheMiddle_ShouldPass() throws SQLException {
        //when
        String expected = "[]";
        String actual = sqlRepository.peopleWhoHasTheLetterBInTheMiddle().toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void homelessPeople_ShouldPass() throws SQLException {
        //when
        String expected = "[(id:1, Kathy Sieara, age:16)]";
        String actual = sqlRepository.homelessPeople().toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void listOfCitizensOnProspectPravdi_ShouldPass() throws SQLException {
        //when
        String expected = "[(id:2, firstName: Leo, lastName: Torn), (id:3, firstName: Max, lastName: Ukupnik)]";
        String actual = sqlRepository.listOfCitizensOnPravdiAvenue().toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void listOfAllStreetsAndCountPersonsOnStreet_ShouldPass() throws SQLException {
        //when
        String expected = "{Pravdi Avenue=2}";
        String actual = sqlRepository.listOfAllStreetsAndCountPersonsOnStreet().toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void listOfStreetsWithSixLetters_ShouldPass() throws SQLException {
        //when
        String expected = "[Bazhan]";
        String actual = sqlRepository.listOfStreetsWithSixLetters().toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void listOfStreetsWhereLessThanThreePersons_ShouldPass() throws SQLException {
        //when
        String expected = "[Pravdi Avenue]";
        String actual = sqlRepository.listOfStreetsWhereLessThanThreePersons().toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    public void prepareForTesting() {
        try {
            sqlRepository.insertDefault3Persons();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}